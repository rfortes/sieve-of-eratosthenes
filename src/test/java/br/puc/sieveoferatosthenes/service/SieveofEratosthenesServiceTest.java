package br.puc.sieveoferatosthenes.service;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author rfortes on 2019-07-04
 */
public class SieveofEratosthenesServiceTest {

  @Test
  public void shouldRetrivePrimes(){
    List<Integer> primes = SieveofEratosthenesService.getPrimes(1000000000);

    System.out.println(primes.get(primes.size()-1));

    Assert.assertNotNull(primes);

  }

}
