package br.puc.sieveoferatosthenes.service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author rfortes on 2019-07-04
 */
public class SieveofEratosthenesService {

  public static List<Integer> getPrimes(int quantity){

    boolean prime[] = new boolean[quantity + 1];
    List<Integer> primes = new ArrayList<>();

    for(int i = 0; i < quantity; i++){
      prime[i] = true;
    }

    for(int p = 2; p*p <= quantity; p++){
      if(prime[p] == true){
        for(int i = p*p; i <= quantity; i += p)
          prime[i] = false;
      }
    }

    for(int i = 2; i <= quantity; i++)
    {
      if(prime[i] == true)
        primes.add(i);
    }

    return primes;
  }

}
