package br.puc.sieveoferatosthenes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SieveOfEratosthenesApplication {

  public static void main(String[] args) {
    SpringApplication.run(SieveOfEratosthenesApplication.class, args);
  }

}
